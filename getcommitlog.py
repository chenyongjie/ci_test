import re
import os
import requests
import json
import sys
 
 
def getContent():
    #获取提交记录信息
    content = (os.popen("git log --pretty=format:\"%an-&%h-&%s\" -1 ").read()).split("-&")
    return content
 
''' 
def postDingDing(content):
    print(sys.argv)
    project_url = sys.argv[1]
    branch_url = sys.argv[2]
    project_name = sys.argv[3]
    commit_url = sys.argv[4]
    branch_name = sys.argv[5]
    name,shortcodenum,explain = content
    dingdingurl = ["https://oapi.dingtalk.com/robot/send?access_token=你的机器人access_token"]
headers = {'Content-Type': 'application/json'}
String_textMsg = {
    "msgtype": "markdown",
    "markdown": {
        "title" : "jbtest",
        "text":   "#### "+name+" 推送到了  ["+project_name+"]("+project_url+")"+"  的["+branch_name+"]("+branch_url+")  分支\n>"
        +name+" -["+shortcodenum+"]("+commit_url+")"+"  "+explain
    }
}
requests.packages.urllib3.disable_warnings()
String_textMsg = json.dumps(String_textMsg)
for i in dingdingurl:
    requests.post(i, data=String_textMsg, headers=headers, verify=False)
print(name,shortcodenum,explain,project_url,branch_url,project_name,commit_url,branch_name)'''

feat = []
newop = []
perf = []
fix = []
commitmsg = []
commit_regx = '[a-z\d]{7}'
jira_regx = '\[\w+-\d+\]'


def classify(msg, flag):
    url = 'http://jira.cambricon.com/browse/'
    commitid = re.match(commit_regx, msg)
    # may have many jira_id
    JIRA = re.findall(jira_regx, msg)
    #get type
    res = re.search(flag, msg)
    temp = ''
    if res != None:
        if len(JIRA) != 0:
            temp = msg[res.span()[0]:] + ' (' + commitid.group() + '    '
            for jira in JIRA:
                url = url + jira.lstrip('[').rstrip(']')
                temp = temp + '[' + jira + ']' + '(' + url + ')'
                url = 'http://jira.cambricon.com/browse/'
            temp = temp + ')'
        else:
            temp = msg[res.span()[0]:] + ' (' + commitid.group() + ')'
    if flag == 'feat *\(' and len(temp) != 0:
        feat.append(temp)
    if flag == 'newOp *\(' and len(temp) != 0:
        newop.append(temp)
    if flag == 'fix *\(' and len(temp) != 0:
        fix.append(temp)
    if flag == 'perf *\(' and len(temp) != 0:
        perf.append(temp)


def classify_msg(msg):
    classify(msg, 'feat *\(')
    classify(msg, 'newOp *\(')
    classify(msg, 'perf *\(')
    classify(msg, 'fix *\(')


def printCHANGELOG():
    if os.path.exists('CHANGELOG.md'):
        os.remove('CHANGELOG.md')
    with open('CHANGELOG.md', 'a+') as f:
        f.write('##### Features\n')
        for i in newop:
            f.write('--' + i + '\n')
        for i in feat:
            f.write('--' + i + '\n')
        for i in perf:
            f.write('--' + i + '\n')
        f.write('##### Bug Fixes\n')
        for i in fix:
            f.write('--' + i + '\n')


#get commit message between [tag1,tag2]
def get_log_tag(tag1, tag2):
    os.system('git log --oneline %s...%s>commitmsg.txt' % (tag1, tag2))
    global commitmsg
    with open('commitmsg.txt', 'r') as f:
        commitmsg = f.read().splitlines()
    os.remove('commitmsg.txt')


#get commit message between [id1,id2]
def get_log_commit(id1, id2):
    os.system('git log --oneline>commitmsg.txt')
    global commitmsg
    start = -1
    end = -1
    with open('commitmsg.txt', 'r') as f:
        commitmsg = f.read().splitlines()
    os.remove('commitmsg.txt')
    for i in range(len(commitmsg)):
        if commitmsg[i].find(id2) != -1:
            start = i
        if commitmsg[i].find(id1) != -1:
            end = i
    if start == -1 or end == -1:
        print('--   commit id is wrong')
        sys.exit(1)
    commitmsg = commitmsg[start:end + 1]

# 支持commit id和tag, 默认使用commit id
# python3 changelog.py  7ffdd9d 68c0bc1
# python3 changelog.py  7ffdd9d 68c0bc1 --commitid
# python3 changelog.py v1.3.3 v1.4.3 --tag
def main():
    if len(sys.argv) < 3:
        print('--   missing parameter')
        sys.exit(1)

    #default
    if len(sys.argv) == 3:
        get_log_commit(sys.argv[1], sys.argv[2])
    elif sys.argv[3] == '--commitid':
        get_log_commit(sys.argv[1], sys.argv[2])
    elif sys.argv[3] == '--tag':
        get_log_tag(sys.argv[1], sys.argv[2])

    for t in commitmsg:
        classify_msg(t)
    printCHANGELOG()

if __name__ == "__main__":
    content = os.popen("git log --pretty=format:\"%an-&%h-&%s\" -1 ").read()
    #with open('CHANGELOG.md', 'a+') as f:
    #with open('/home/gitlab-runner/builds/iHM57z-f/0/chenyongjie/ci_test/CHANGELOG.md', 'a+') as f:
    with open(sys.argv[1], 'a+') as f:
        f.write('##### Features\n')
        f.write(content)
    print("content:",content)
   
        
    print('write')
   # postDingDing(content)
